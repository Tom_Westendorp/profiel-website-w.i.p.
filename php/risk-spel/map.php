
<?php
include_once("cell.php");
  class map
  {
      public $locations;
      public $rowamount;
      function __construct($rows,$colums)
      {
          $this->rowamount = $colums;
          $this->createnewmap($rows,$colums);
      }
      function createnewmap($rows,$colums){
         $this->locations = array();
          $totalamount = $rows*$colums;
          for($i=1; $i <= $totalamount ; $i++) {
              $this->locations[$i] = new cell($i);
          }
      }
      function show(){
          $x = 1;
         
          foreach ($this->locations as $key => $cell){
              if($x == 1){echo"<tr>";}
            
               $cell->show();
         if($x==$this->rowamount) {echo"</tr>";$x=0;}  
              $x++;
          }
         
        
      }
    
function Surrounding($pos){
        if ($pos>$this->rowamount) {
        if($pos%$this->rowamount !=1){$this->locations[$pos-$this->rowamount-1]->selectable = true;}
        $this->locations[$pos-$this->rowamount]->selectable = true;//pos - row
        if($pos%$this->rowamount!=0){$this->locations[$pos-$this->rowamount+1]->selectable = true;}
        }

        if($pos%$this->rowamount!=1){$this->locations[$pos-1]->selectable = true;}
        if($pos%$this->rowamount!=0){$this->locations[$pos+1]->selectable = true;}

        if($pos <count($this->locations)-$this->rowamount){
        if($pos%$this->rowamount!=1){$this->locations[$pos+$this->rowamount-1]->selectable = true;}
        $this->locations[$pos+$this->rowamount]->selectable = true;//pos - row
        if($pos%$this->rowamount!=0){$this->locations[$pos+$this->rowamount+1]->selectable = true;}
        }
    }
      function checksurounding($array,$pos){
           if ($pos>$this->rowamount) {
        if($pos%$this->rowamount !=1){array_push($array,$pos+$this->rowamount-1);}
        array_push($array,$pos+$this->rowamount);//pos - row
        if($pos%$this->rowamount!=0){array_push($array,$pos+$this->rowamount+1);}
        }

        if($pos%$this->rowamount!=1){array_push($array,$pos-1);}
        if($pos%$this->rowamount!=0){array_push($array,$pos+1);}

        if($pos <count($this->locations)-$this->rowamount){
        if($pos%$this->rowamount!=1){array_push($array,$pos-$this->rowamount-1);}
        array_push($array,$pos-$this->rowamount);//pos - row
        if($pos%$this->rowamount!=0){array_push($array,$pos-$this->rowamount+1);}
        }
                                                return $array;
    }
      function resetmap(){
          foreach ($this->locations as $cell){
              $cell->selectable = false;
          }
      }
  }

?>
