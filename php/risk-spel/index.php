<?php
$temp =30;

include_once("game.php");
session_start();
$rows = 6;
$columns=6;
$colors = array(1 => "yellow",2 => "blue",3 => "orange",4 => "aquamarine");//,5 => "silver");
$playeramount = count($colors) ;


if(!isset($_SESSION['game'])){
    $game = new game($rows,$columns,$playeramount,$colors);

}else{
    $game = $_SESSION['game'];
}



if(isset($_GET['options'])){
   $game->setoptie($_GET['options']);
}

if(isset($_GET['click'])){
   $game->setmove($_GET['click']);
}
?>
<!doctype html>
<html lang="nl">
<head>
    <meta http-equiv="content-type"
          content="text/html;
                   charset=UTF-8" />
    <style>
        
        
        html{
            background-color: darkslategrey;
        }
        
        image{
               width: 50px;
            height: 50px;
            }
        input{
            width:50%;
        }
            input.full{width:100%;}
        
        td{
            border-radius: 15%;
          
            width: 50px;
            height: 50px;
          background-color: firebrick;
            border: 2px solid white;
         
          
            text-align:center;
        }
        td.selectAble{border: 3px solid yellow;}
        
        box{
            display: block;
                    width: calc(100% / <?php echo $playeramount; ?>);
                    float: left;
        }
        td.selectAble{border: 3px solid yellow;}
        <?php
        
        for ($i=1; $i <=  count($colors); $i++){
          echo  " td.".$colors[$i]."{background-color:". $colors[$i].";}";
            echo  " box.".$colors[$i]."{background-color:". $colors[$i].";}";
        }
        ?>
        
        
    </style>
    </head>
    <body>
    <?php
     
        $game->show();
        
        
        $_SESSION['game'] = $game;
        ?>
        <script type="text/javascript">
    var total = <?php echo $rows*$columns; ?>;
    for(var i=1;i<=String(total);i++){
      document.getElementById(String(i)).onclick = Go_click;
    }
    function Go_click(){
      window.location.href = "index.php?click="+String(this.id);
    }
    </script>
        
        
    </body>
</html>
