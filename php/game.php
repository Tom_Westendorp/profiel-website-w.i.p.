<?php
	include_once("karakter.php");

	class game
	{
		public $Player1;
		public $Player2;
		private $turn;
		public $gameActive;

		function __construct($levens)
		{
			$this->Player1 = new karakter("Jan", $levens);
			$this->Player2 = new karakter("Bo", $levens);
            $this->turn = true;
			$this->gameActive = true;
		}
		function Turn(){
			if($this->turn){ 
				$damege = $this->Player1->Attack();
				if($this->Player2->GetDamage($damege)){}else{$this->Win($this->Player1->Naam);}
			}else{ 
				$damege = $this->Player2->Attack();
				if($this->Player1->GetDamage($damege)){}else{$this->Win($this->Player2->Naam);}

			$this->turn = !$this->turn;
			return $damege;
		}
		}

		function Win($naam){
			echo $naam . " heeft gewonnen!";
			$this->gameActive = false;
		}
	}
?>