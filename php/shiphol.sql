-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Gegenereerd op: 20 mrt 2019 om 11:26
-- Serverversie: 10.1.36-MariaDB
-- PHP-versie: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shiphol`
--
CREATE DATABASE IF NOT EXISTS `shiphol` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `shiphol`;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `gebruiker`
--

DROP TABLE IF EXISTS `gebruiker`;
CREATE TABLE `gebruiker` (
  `ID` int(11) NOT NULL,
  `ID_klachtsoort` int(11) NOT NULL,
  `postcode` varchar(20) NOT NULL,
  `datum` date NOT NULL,
  `tijd` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `gebruiker`
--

INSERT INTO `gebruiker` (`ID`, `ID_klachtsoort`, `postcode`, `datum`, `tijd`) VALUES
(28, 6, '1912 CC', '2019-03-20', '22:31'),
(29, 8, '2523 AZ', '2019-03-20', '22:31'),
(30, 8, '2523 AZ', '2019-03-20', '22:25');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `klachtsoort`
--

DROP TABLE IF EXISTS `klachtsoort`;
CREATE TABLE `klachtsoort` (
  `ID_klachtsoort` int(11) NOT NULL,
  `soort` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `klachtsoort`
--

INSERT INTO `klachtsoort` (`ID_klachtsoort`, `soort`) VALUES
(6, 'milieu'),
(7, 'geluid'),
(8, 'veiligheid');

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `gebruiker`
--
ALTER TABLE `gebruiker`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID_klachtsoort` (`ID_klachtsoort`),
  ADD KEY `ID_klachtsoort_2` (`ID_klachtsoort`);

--
-- Indexen voor tabel `klachtsoort`
--
ALTER TABLE `klachtsoort`
  ADD PRIMARY KEY (`ID_klachtsoort`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `gebruiker`
--
ALTER TABLE `gebruiker`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT voor een tabel `klachtsoort`
--
ALTER TABLE `klachtsoort`
  MODIFY `ID_klachtsoort` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
