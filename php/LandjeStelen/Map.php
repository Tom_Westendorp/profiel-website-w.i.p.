<?php
include_once("Vakje.php");

/**
 *  The Map
 */
class Map
{ 
  public $Locations;
  private $AmountPerRow;

  function __construct($rows,$columns)
  {
    $this->CreateNewMap($rows,$columns);
  }

  public function CreateNewMap($rows,$columns){
    $this->Locations = array();
    $this->AmountPerRow = $columns;
    $total = $rows*$columns;
    for ($i=1; $i <= $total; $i++) {
      $this->Locations[$i] = new Square($i);
    }
  }

  public function ShowMenu($Players,$disable,$optie,$turn){
    echo "<thead><tr><th colspan='".$this->AmountPerRow."'>";
    for ($i=1; $i <= count($Players); $i++) {
      echo "<box class='".$Players[$i]."'>";
      echo "<p>Player<br/>".$Players[$i] . "<br/></p>";

      echo "<input type='button' onclick='window.location.href=`index.php?Option=Move`;' value='Move' class='full' ";
      if($optie=='Move'&&$i==$turn){echo "style='background-color:yellow;'";}
      echo " ".$disable[$i]."/><br/>";
      echo "<input type='button' onclick='window.location.href=`index.php?Option=Build`;' value='Build' ";
      if($optie=='Build'&&$i==$turn){echo "style='background-color:yellow;'";}
      echo " ".$disable[$i]."/>";
      echo "<input type='button' onclick='window.location.href=`index.php?Option=Char`;' value='Char' ";
      if($optie=='Char'&&$i==$turn){echo "style='background-color:yellow;'";}
      echo " ".$disable[$i]."/>";
      echo "</box>";
    }
    echo "</th></tr></thead>";
  }
  public function ShowBoard(){
    echo "<tbody>";
    $nr = 1;
    foreach ($this->Locations as $key => $Square) {
      if($nr == 1){echo "<tr>";}
      $Square->Show();
      if($nr == $this->AmountPerRow){echo "</tr>";$nr = 0;}
      $nr++;
    }
    echo "</tbody>";
  }

  public function ChooseTile($id){
    $WAARDE = "SelectAble";

    if($id>$this->AmountPerRow){
      if($id%$this->AmountPerRow!=1){$this->Locations[$id-$this->AmountPerRow-1]->ClickAble = $WAARDE;}
      $this->Locations[$id-$this->AmountPerRow]->ClickAble = $WAARDE;
      if($id%$this->AmountPerRow!=0){$this->Locations[$id-$this->AmountPerRow+1]->ClickAble = $WAARDE;}
    }
    if($id%$this->AmountPerRow!=1){$this->Locations[$id-1]->ClickAble = $WAARDE;}
    if($id%$this->AmountPerRow!=0){$this->Locations[$id+1]->ClickAble = $WAARDE;}

    if($id<(count($this->Locations)-$this->AmountPerRow)+1){
      if($id%$this->AmountPerRow!=1){$this->Locations[$id+$this->AmountPerRow-1]->ClickAble = $WAARDE;}
      $this->Locations[$id+$this->AmountPerRow]->ClickAble = $WAARDE;
      if($id%$this->AmountPerRow!=0){$this->Locations[$id+$this->AmountPerRow+1]->ClickAble = $WAARDE;}
    }
  }

  public function GiveSurrounding($array,$id){
    if($id>$this->AmountPerRow){
      if($id%$this->AmountPerRow!=1){array_push($array,$this->Locations[$id-$this->AmountPerRow-1]->ID);}
      array_push($array,$this->Locations[$id-$this->AmountPerRow]->ID);
      if($id%$this->AmountPerRow!=0){array_push($array,$this->Locations[$id-$this->AmountPerRow+1]->ID);}
    }
    if($id%$this->AmountPerRow!=1){array_push($array,$this->Locations[$id-1]->ID);}
    if($id%$this->AmountPerRow!=0){array_push($array,$this->Locations[$id+1]->ID);}

    if($id<(count($this->Locations)-$this->AmountPerRow)+1){
      if($id%$this->AmountPerRow!=1){array_push($array,$this->Locations[$id+$this->AmountPerRow-1]->ID);}
      array_push($array,$this->Locations[$id+$this->AmountPerRow]->ID);
      if($id%$this->AmountPerRow!=0){array_push($array,$this->Locations[$id+$this->AmountPerRow+1]->ID);}
    }
    return $array;
  }
}
 ?>
