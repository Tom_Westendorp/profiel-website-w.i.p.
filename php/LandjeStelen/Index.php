<?php
include_once("Game.php");
session_start();

$rows = 6;
$columns = 6;
$AmountPlayers = 6;
$colors = array(0 => "",1 => "green",2 => "red",3 => "blue",4 => "purple",5 => "orange",6 => "brown",7 => "pink",8 => "tomato");

if(isset($_SESSION['Game'])){
  $Game = $_SESSION['Game'];
}else{
  $Game = new Gamemanager($rows,$columns,$colors,$AmountPlayers);
}

if(isset($_GET['Option'])){
  $Game->SetOption($_GET['Option']);
}
if(isset($_GET['click'])){
  $Game->Select($_GET['click']);
}
if(isset($_GET['reset'])){session_destroy();header("location:index.php");}
 ?>

<html lang="nl" dir="ltr">
  <head>
    <meta charset="utf-8">
    <style type="text/css">
    img{height: 48px;}
    box{display: block;float: left;
    width: calc(100% / <?php echo $AmountPlayers;?>);}
    .full{width: 100%;}
    input{width: 50%;}
    td{
      width: 50px;
      height: 50px;
      background-color: #999;
      text-align: center;
      border: 1px solid black;
    }
    td.SelectAble{border: 1px solid yellow;}
    <?php
    foreach ($colors as $key => $color){
      echo "td.".$color."{background-color: ".$color.";}";
      echo "box.".$color."{background-color: ".$color.";}";
    }
    ?>
    </style>
  </head>
  <body>
    <a href="index.php?reset="/>reset</a>
    <center>
      <table>
        <?php $Game->Show(); ?>
      </table>
    </center>

    <script type="text/javascript">
    var total = <?php echo $rows*$columns; ?>;
    for(var i=1;i<=String(total);i++){
      document.getElementById(String(i)).onclick = Go_click;
    }
    function Go_click(){
      window.location.href = "index.php?click="+String(this.id);
    }
    </script>
    <?php $_SESSION['Game'] = $Game;?>
  </body>
</html>
