<?php

/**
 *  vakje
 */
class Square
{
  public $ID;
  public $Color;
  public $ClickAble;

  function __construct($id)
  {
    $this->ID = $id;
    $this->Color = "";
    $this->ClickAble = "";
  }

  public function Show(){
    echo "<td class='".$this->Color." ".$this->ClickAble."' id='".$this->ID."'>"."</td>";
  }

  public function CheckEmpty(){
    if($this->Color==""){
      return true;
    }else{
      return false;
    }
  }
}
/**
 *  HomeBase
 */
class Home extends Square
{
  private $lvl;
  function __construct($id,$color,$lvl)
  {
    $this->ID = $id;
    $this->lvl = $lvl;
    $this->Color = $color;
    $this->ClickAble = "";

  }
  public function Show(){
    echo "<td class='".$this->Color." ".$this->ClickAble."' id='".$this->ID."'>";
    if($this->lvl==1){echo "<img src='Images/Tower1.png'/>";}
    if($this->lvl==2){echo "<img src='Images/Tower2.png'/>";}
    if($this->lvl>=3){echo "<img src='Images/Tower3.png'/>";}
    echo "</td>";
  }
}
/**
 *    Popeke
 */
class Character extends Square
{
  private $lvl;

  function __construct($id,$color)
  {
    $this->ID = $id;
    $this->Color = $color;
    $this->ClickAble = "";
    $this->lvl = 1;
  }
  public function Show(){
    echo "<td class='".$this->Color." ".$this->ClickAble."' id='".$this->ID."'>";
    if($this->lvl==1){echo "<img src='Images/Character.png'/>";}
    if($this->lvl==2){echo "<img src='Images/Fighter.png'/>";}
    if($this->lvl>=3){echo "<img src='Images/Knight.png'/>";}
    echo "</td>";
  }
  public function LvlUp(){
    $this->lvl++;
  }
  public function CheckLvl(){
    return $this->lvl;
  }
}

 ?>
