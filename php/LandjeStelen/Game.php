<?php
include_once("Map.php");
/**
 *  Gamemanager
 */
class Gamemanager
{
  public $Map;
  public $Players;
  public $Disable; // odat maar een kan moven
  public $Option; //welke game mode move, build , spawn
  private $Turn; //wie aan de beurt is 

  function __construct($rows,$columns,$colors,$AmountPlayers)
  {
    $this->Map = new Map($rows,$columns);
    $this->CreatePlayers($colors,$AmountPlayers);
    $this->Turn = rand(1,$AmountPlayers);
    $this->Disable[$this->Turn] = "";
    $this->Option = 'Move';
  }

  function CreatePlayers($colors,$AmountPlayers){
    $this->Players = array();
    $this->Disable = array(0 => "");
    for ($i=1; $i <= $AmountPlayers; $i++) {
      array_push($this->Disable,"disabled");
      $this->Players[$i] = $colors[$i];
      $check = true;
      while($check){
        $finalcheck = true;
        $randomPos = rand(1,count($this->Map->Locations));
        if($this->Map->Locations[$randomPos]->CheckEmpty()){
          $temp_array = $this->Map->GiveSurrounding(array(),$randomPos);
          foreach ($temp_array as $pos) {
            if(!$this->Map->Locations[$pos]->CheckEmpty()){
              $finalcheck = false;
            }
          }
          if($finalcheck){
            $this->Map->Locations[$randomPos] = new Home($randomPos,$colors[$i],3);
            $check = false;
          }
        }
        //echo "Speler " .$colors[$i]. " zit op positie " .$randomPos. "<br/>";
      }
    }
  }
  function Show(){
    $this->GameMode();
    $this->Map->ShowMenu($this->Players,$this->Disable,$this->Option,$this->Turn);
    $this->Map->ShowBoard();
  }
  function SetOption($optie){
    $this->ResetMove();
    $this->Option = $optie;
  }
  function NextPlayer(){
    if($this->Turn==count($this->Players)){
      $this->Turn=1;
    }else{
      $this->Turn++;
    }
    $this->ResetButtons();
    $this->ResetMove();
  }
  function ResetButtons(){
    for ($i=1; $i <= count($this->Players); $i++) {
      $this->Disable[$i] = "disabled";
    }
    $this->Disable[$this->Turn] = "";
  }
  function ResetMove(){
    foreach ($this->Map->Locations as $square) {
      $square->ClickAble = "";
    }
  }
  function GameMode(){
    switch ($this->Option) {
      case 'Move':
      foreach ($this->Map->Locations as $square) {
        if(is_a($square,"Character")){
          if($square->Color==$this->Players[$this->Turn]){
            $this->Map->ChooseTile($square->ID);
          }
        }
      }
        break;
      case 'Build':
      foreach ($this->Map->Locations as $square) {
        if(is_a($square,"Character")){
          if($square->Color==$this->Players[$this->Turn]){
            $this->Map->ChooseTile($square->ID);
          }
        }
      }
        break;
      case 'Char':
      foreach ($this->Map->Locations as $square) {
        if(is_a($square,"Home")){
          if($square->Color==$this->Players[$this->Turn]){
            $this->Map->ChooseTile($square->ID);
          }
        }
      }
        break;
    }
  }
  function Select($id){
    switch ($this->Option) {
      case 'Move':
      $legeArray = array();
      $arrayIDs = $this->Map->GiveSurrounding($legeArray,$id);
      foreach ($arrayIDs as $ID) {
        if(is_a($this->Map->Locations[$ID],"Character")){
          if($this->Map->Locations[$ID]->Color==$this->Players[$this->Turn]){
            $this->checkMoveCharcter($id,$ID);
            break;
          }
        }
      }
        break;
      case 'Build':
      $legeArray = array();
      $arrayIDs = $this->Map->GiveSurrounding($legeArray,$id);
      foreach ($arrayIDs as $ID) {
        if(is_a($this->Map->Locations[$ID],"Character")){
          if($this->Map->Locations[$ID]->Color==$this->Players[$this->Turn]){
            if($this->Map->Locations[$id]->Color==$this->Players[$this->Turn]){
              $this->Map->Locations[$id]= new Home($id,$this->Players[$this->Turn],1);
              $this->NextPlayer();
            }
          }
        }
      }
        break;
      case 'Char':
        $legeArray = array();
        $arrayIDs = $this->Map->GiveSurrounding($legeArray,$id);
        foreach ($arrayIDs as $ID) {
          if(is_a($this->Map->Locations[$ID],"Home")){
            if($this->Map->Locations[$ID]->Color==$this->Players[$this->Turn]){
              $this->Map->Locations[$id] = new Character($id,$this->Players[$this->Turn]);
              $this->NextPlayer();
            }
          }
        }
        break;
    }
  }

  private function checkMoveCharcter($new,$old){
    if(is_a($this->Map->Locations[$new],"Character")){
      if($this->Map->Locations[$new]->Color!=$this->Players[$this->Turn]){
        if($this->Map->Locations[$new]->CheckLvl()<=$this->Map->Locations[$old]->CheckLvl()){
          $this->moveCharcter($new,$old);
        }
      }else{
        $this->Map->Locations[$new]->LvlUp();
        $this->Map->Locations[$old] = new Square($old);
        $this->Map->Locations[$old]->Color=$this->Players[$this->Turn];
        $this->NextPlayer();
      }
    }elseif(is_a($this->Map->Locations[$new],"Home")){
      if($this->Map->Locations[$new]->Color!=$this->Players[$this->Turn]){
        $this->moveCharcter($new,$old);
      }
    }else{
      $this->moveCharcter($new,$old);
    }
  }
  private function moveCharcter($new,$old){
    $this->Map->Locations[$new] = $this->Map->Locations[$old];
    $this->Map->Locations[$new]->ID = $new;
    $this->Map->Locations[$old] = new Square($old);
    $this->Map->Locations[$old]->Color=$this->Players[$this->Turn];
    $this->NextPlayer();
  }
  private function checkWin(){
    $aantal = array();
    for ($k=1; $k <= $this->Players; $k++) {
      $aantal[$k] = 0;
    }
    foreach ($this->Map->Locations as $square) {
      if(is_a($square,"Home")){
        for ($k=1; $k <= $this->Players; $k++) {
          if($square->Color==$this->Players[$k]){$aantal[$k]++;}
        }
      }
    }
    for ($k=1; $k <= $this->Players; $k++) {
      if($aantal[$k]==0){
        if($this->Turn==$k){
          $this->NextPlayer();
        }
      }
    }
  }
}



 ?>
