<html>
<head>
  <?php include_once("Game.php"); ?>
</head>
<body>
  <?php
  $Game = new game(25);

   ?>
  <center>
    <table>
      <thead>
        <tr>
          <th><?php echo $Game->Player1->Naam;?></th>
          <th></th>
          <th><?php echo $Game->Player2->Naam;?></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td><?php echo $Game->Player1->GetLevens();?></td>
          <td></td>
          <td><?php echo $Game->Player2->GetLevens();?></td>
        </tr>
        <?php
        while($Game->gameActive){
          $schade = $Game->Turn();
        ?>
        <tr>
          <td><?php echo $Game->Player1->GetLevens();?></td>
          <td><?php echo $schade; ?></td>
          <td><?php echo $Game->Player2->GetLevens();?></td>
        </tr>
      <?php } ?>
      </body>
    </table>
  </center>
</body>
</html>
