select FR_Factuurregelid as factuurregel, FR_Productid as productid, FR_Aantal as Aantal, FR_Prijs as Prijs, FR_Aantal*FR_Prijs as Totaal from factuurregel

select P_Productnaam, FR_Factuurregelid as factuurregel, 
FR_Productid as productid, FR_Aantal as Aantal,
FR_Prijs as Prijs, FR_Aantal*FR_Prijs as Totaal 
from factuurregel inner join product on 
factuurregel.FR_Productid = product.P_productid 

select P_Productnaam as Product, FR_Factuurid as factuur, 
FR_Productid as productid, FR_Aantal as Aantal,
FR_Prijs as Prijs, FR_Aantal*FR_Prijs as Totaal 
from factuurregel inner join product on 
factuurregel.FR_Productid = product.P_productid 

select P_Productnaam as Product, FR_Factuurid as factuur, 
FR_Productid as productid, FR_Aantal as Aantal,
FR_Prijs as Prijs, FR_Aantal*FR_Prijs as Totaal 
from factuurregel inner join product on 
factuurregel.FR_Productid = product.P_productid 
order by FR_Factuurid,P_Productnaam

select FR_Factuurid as factuur, 
 FR_Aantal as Aantal,
FR_Prijs as Prijs, FR_Aantal*FR_Prijs as Totaal 
from factuurregel inner join product on 
factuurregel.FR_Productid = product.P_productid 
order by FR_Factuurid,P_Productnaam

select FR_Factuurid as factuur, 
 FR_Aantal as Aantal,
 FR_Aantal*FR_Prijs as Totaal 
from factuurregel 
order by FR_Factuurid

select FR_Factuurid as factuur, 
 count(FR_Aantal) as Aantal,
 count(FR_Aantal * FR_prijs) as totaal
 from factuurregel group by FR_Factuurid

select FR_Factuurid as factuur, 
 SUM(FR_Aantal) as Aantal,
 SUM(FR_Aantal * FR_prijs) as totaal
 from factuurregel group by FR_Factuurid

select FR_Factuurid as factuur, 
 SUM(FR_Aantal) as Aantal,
 SUM(FR_Aantal * FR_prijs) as totaal
 from factuurregel 
 fr inner join factuur f on fr.FR_Factuurid = f.F_Factuurid
 group by FR_Factuurid

select k_Bedrijfsnaam, FR_Factuurid as factuur, F_factuurdatum, F_klant_id,
 SUM(FR_Aantal) as Aantal,
 SUM(FR_Aantal * FR_prijs) as totaal
 from factuurregel 
 fr inner join factuur f on fr.FR_Factuurid = f.F_Factuurid
 inner join klanten k on f.F_klant_ID = k.K_Klant_ID
 group by K_Bedrijfsnaam, FR_Factuurid, F_factuurdatum, F_klant_id

select k_Bedrijfsnaam, 

 SUM(FR_Aantal * FR_prijs) as totaal
 from factuurregel 
 fr inner join factuur f on fr.FR_Factuurid = f.F_Factuurid
 inner join klanten k on f.F_klant_ID = k.K_Klant_ID
 group by K_Bedrijfsnaam

select P_productnaam,

 SUM(FR_Aantal * FR_prijs) as totaal
 from factuurregel 
 fr inner join product p on fr.FR_Productid = p.P_productid

 group by P_productnaam


select k_Bedrijfsnaam, 

 SUM(FR_Aantal * FR_prijs) as totaal
 from factuurregel 
 fr inner join factuur f on fr.FR_Factuurid = f.F_Factuurid
 inner join klanten k on f.F_klant_ID = k.K_Klant_ID
 group by K_Bedrijfsnaam
 having SUM(FR_Aantal*FR_Prijs) >=500


select k_Bedrijfsnaam, 

 SUM(FR_Aantal * FR_prijs) as Totaal
 from factuurregel 
 fr inner join factuur f on fr.FR_Factuurid = f.F_Factuurid
 inner join klanten k on f.F_klant_ID = k.K_Klant_ID
 group by K_Bedrijfsnaam
 having Totaal >=500
 order by Totaal DESC

select k_Bedrijfsnaam as bedrijf, f_factuurid
 from klanten k 
 inner join factuur f on  k.K_klant_ID = f.F_Klant_ID
 order by bedrijf

select k_Bedrijfsnaam as bedrijf, f_factuurid
 from klanten k 
 left outer join factuur f on  k.K_klant_ID = f.F_Klant_ID
 order by bedrijf

select k_Bedrijfsnaam as bedrijf, f_factuurid
 from klanten k 
 left join factuur f on  k.K_klant_ID = f.F_Klant_ID
 where f_factuurid is NULL order by bedrijf